if (!typeof(update_urls) == 'undefined') update_urls();
var map_res;
var companies;

var current_url = [location.protocol, '//', location.host, location.pathname].join('');
var layer_url = 'https://vector.mapzen.com/osm/all/{z}/{x}/{y}.png?api_key=vector-tiles-' + API_KEY;
var layer_url = 'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png';
var main_layer = L.tileLayer(layer_url, {
  attribution: '<a href="https://www.mapzen.com/rights">Attribution.</a>. Data &copy;<a href="https://openstreetmap.org/copyright">OSM</a> contributors.'
});

var markers;
var map_layers = {};
var icons = {};
// icon_on
// var icons_on = {};
var map_initialized;

default_detail = "<div class='body center'><img class='masterTooltip' src='" + img_path + "ajax-loader.gif' title='Chargement...'/></div><div class='foot'></div>";

var no_detail = "<i>Vous devez sélectionner une ferme sur la carte ou dans la liste de résultat pour voir apparaître le détail ici.</i>"

var is_mobile = false;
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) is_mobile = true;

var extra_json_data  = [
    {
        name:"Départements",
        url: json_path + "departements.geojson",
        style:function(feature) {
            c_zoom = map_res.getZoom();
            fillOpacity = 0.35;
            if (c_zoom >= 14){
                fillOpacity = 0.15;
            } else if (c_zoom >= 12){
                fillOpacity = 0.25;
            }
            return {
                "color": "#000",
                "weight": 1,
                "opacity": 1,
                "fillColor": feature.properties.color,
                "fillOpacity": fillOpacity
            };
        }
    }
]

function update_companies(){
    if (markers){
        map_res.removeLayer(markers);
    }
    markers = new L.MarkerClusterGroup({"maxClusterRadius": 10});

    markers.on('clusterclick', function (a) {
        setTimeout(function(){
            a.layer.spiderfy();
        }, 1000);
    });

    map_res.addLayer(markers);
    for (i=0 ; i<companies.length ; i++) {
        var c_comp = companies[i];
        if (c_comp['geometry'] && c_comp['geometry']['coordinates']){
            var plotll = new L.LatLng(c_comp['geometry']['coordinates'][1], c_comp['geometry']['coordinates'][0], true);
            if (c_comp['properties']['icon'] && !icons[c_comp['properties']['icon']]){
                icons[c_comp['properties']['icon']] = new L['icon']({
                    iconUrl:img_path + c_comp['properties']['icon'],
                    iconSize: [26, 30],
                    iconAnchor: [12, 30],
                    popupAnchor: [2, -33],
                    hover: false
                    });
                /* // icon_on
                icons_on[c_comp['properties']['icon']] = new L['icon']({
                    iconUrl:img_path + c_comp['properties']['icon'].substr(0, c_comp['properties']['icon'].length - 4) + '-on.png',
                    iconSize: [26, 30],
                    iconAnchor: [12, 32],
                    popupAnchor: [2, -33],
                    hover: true
                    });
                */
            }
            var options = new Array();
            if (c_comp['properties']['icon']){
                options['icon'] = icons[c_comp['properties']['icon']];
            }
            plotmark = new L.Marker(plotll, options);
            /* // icon_on
            c_comp.properties['icon_on'] = icons_on[c_comp['properties']['icon']];
            c_comp.properties['icon'] = icons[c_comp['properties']['icon']];
            */
            plotmark.data = c_comp.properties;
            markers.addLayer(plotmark);
            var content = "<h3>" + c_comp["properties"]["nom"] + "</h3>" + default_detail;
            var popup = L.popup({maxWidth: 480, className: plotmark.data.className}).setContent(content);
            popup.marker = plotmark;
            plotmark.bindPopup(popup);
        }
    }
}

var search_is_empty = false;

var on_autocomplete_change = function(evt, ui){
    if (ui){
        $('#id_select_localisation').val(ui.item.label);
        $('#id_localisation').val(ui.item.id);
        search_is_empty = false;
    } else {
        $('#id_select_localisation').val('');
        $('#id_localisation').val('');
        search_is_empty = true;
    }
    $(".search-form").submit();
}


var autoselect_on_empty = function(){
    if (!search_is_empty) on_autocomplete_change();
}

function init_search(data){
    $("#search").html(data);
    $("#search .select2").select2().on('select2:unselecting', function() {
        $(this).data('unselecting', true);
    }).on('select2:opening', function(e) {
        if ($(this).data('unselecting')) {
            $(this).removeData('unselecting');
            e.preventDefault();
        }
    });
    $('.select2-container').each(function () {
        $(this).find('.select2-search, .select2-focusser').hide();
    });

    $('ul#farm-list').offset({ top: $('#resultats').offset().top + 40});
    load_companies();
    $('#search h4').click(function(){
        var cid = $(this).attr('data-id');
        map_popup(cid);
    });
    $(".search-form input, .search-form select").change(
        function(){
            $(".search-form").submit();
        }
    );
    $("#id_select_localisation").on(
        "autocompleteselect", on_autocomplete_change
    );
    if (search_is_empty) $("#id_select_localisation").focus();
    $(".search-form").submit(function(){
        $.ajax({
                url: form_url,
                type: 'POST',
                data: $(this).serialize(),
                success: function(data) {
                    init_search(data);
                }
            });
        return false;
    });
}

function load_companies(){
    $.ajax({
        dataType: "json",
        url: src_url,
        contentType: "application/json;charset=UTF-8",
        success: function(data) {
            companies = data['features'];
            update_companies();
        }
    });
}

var btn_farm_trigger = function(){
    $("#btn-search").removeClass('selected');
    $("#btn-map").removeClass('selected');
    $("#btn-farm").addClass('selected');
    $("#mobile-detail").show();
    //$("#map-result").hide();
    $("#sidebar").hide();
}


function initmapres(){
    if (map_initialized) return;

    if (is_mobile){
        $('body').addClass('mobile');
    }

    $('#btn-search').click(function(){
        window.history.pushState(null, null, current_url + '#search');
        $("#btn-farm").removeClass('selected');
        $("#btn-map").removeClass('selected');
        $("#btn-search").addClass('selected');
        $("#sidebar").show();
        $("#map-result").hide();
        $("#mobile-detail").hide();
        $('ul#farm-list').offset({ top: $('#resultats').offset().top + 40});
    });

    $('#btn-map').click(function(){
        window.history.pushState(null, null, current_url + '#map');
        $("#btn-farm").removeClass('selected');
        $("#btn-search").removeClass('selected');
        $("#btn-map").addClass('selected');
        $("#map-result").show();
        $("#sidebar").hide();
        $("#mobile-detail").hide();
    });

    $('#btn-farm').click(function(){
       window.history.pushState(null, null, current_url + '#map');
       btn_farm_trigger();
    });

    $(window).on('resize', function(){
        var win = $(this);
        if (win.width() >= 750) {
            $("#map-result").show();
            $("#sidebar").show();
            $("#btn-farm").removeClass('selected');
            $("#btn-search").removeClass('selected');
            $("#btn-map").addClass('selected');
        }
    });

    $("#mobile-detail").html(no_detail);

    map_initialized = true;
    map_layers = {'Default': main_layer};
    map_res = new L.Map('map-result', {layers: [main_layer],
                                       center:new L.LatLng(0,0), zoom:15,
                                       fullscreenControl: true});

    var icons = new Array();

    /* L.easyPrint().addTo(map_res);

    map_res.on('fullscreenchange', function () {
        if (map_res.isFullscreen()) {
            $('.leaflet-control-easyPrint').hide();
        } else{
            $('.leaflet-control-easyPrint').show();
        }
    }); */
    map_res.on('popupclose', function(e) {
        if (is_mobile){
            $("#mobile-detail").removeClass();
            $("#mobile-detail").html(no_detail);
        }
    });
    map_res.on('popupopen', function(e) {
        var popup = e.popup;
        var marker = popup.marker;
        // icon_on
        /*
        markers.eachLayer(function(m){
            if(typeof m.data.id != 'undefined' && m.options.icon.options.hover == true){
                m.setIcon(m.data.icon);
            }
        });
        marker.setIcon(marker.data.icon_on);
        */

        if (is_mobile){
            var px = map_res.project(popup._latlng);
            map_res.panTo(map_res.unproject(px), {animate: true});
            $("#mobile-detail").html($('.leaflet-popup-content').html());
            var classList = $('.leaflet-popup').attr('class').split(/\s+/);
            $("#mobile-detail").removeClass();
            $.each(classList, function(index, item) {
                if (item.substr(0, 3) === 'dpt') {
                    $("#mobile-detail").addClass(item);
                }
            });
            if(marker.data.detail){
                setTimeout(
                    function(){
                        btn_farm_trigger();
                    }, 1500);
            }
        }
        if (!marker.data.detail){
            var url = detail_url.replace('slug', marker.data.id);
            $.ajax({url: url}).done(function(detail){
                marker.data.detail = detail;
                if (!is_mobile){
                    marker.setPopupContent(marker.data.detail);
                } else {
                    $("#mobile-detail").html(marker.data.detail);
                    setTimeout(
                        function(){
                            btn_farm_trigger();
                        }, 1500);
                }
            });
        } else {
            $("#mobile-detail").html(marker.data.detail);
        }
    });
    /* var layer_control_options = {};
    layer_control_options['position'] = 'topleft';
    layer_control = L.control.layers(map_layers, null,
                                layer_control_options).addTo(map_res); */
    layers = [];
    for (idx=0;idx<extra_json_data.length;idx++){
        $.ajax({
            dataType: "json",
            url: extra_json_data[idx]['url'],
            context: extra_json_data[idx],
            success: function(data) {
                var geojsonLayer = new L.GeoJSON(data, {
                    style:this['style']
                });
                geojsonLayer.addTo(map_res);
                layers.push(geojsonLayer);
                // layer_control.addOverlay(geojsonLayer, this['name']);
                var bounds = geojsonLayer.getBounds();
                if (bounds.isValid()){
                    map_res.fitBounds(bounds);
                }
            }
        });
    }

    map_res.on('zoomend', function(e) {
        // $('#debug').html("Debug - zoom : " + map_res.getZoom());
        for (k in layers){
            var layer = layers[k];
            var c_layers = layer.getLayers();
            for (j in c_layers) layer.resetStyle(c_layers[j]);
        }
    });

    $.ajax({
        url: form_url + '?first=true',
        success: function(data) {
            init_search(data);
        }
    });

}

function map_popup(pk){
    if ($("#map-result").is(":visible") != true) $('#btn-map').click();
    markers.eachLayer(function(layer){
        if (layer.data.id == pk){
            if (!is_mobile){
                map_res.setView(layer.getLatLng(), 13);
                layer.openPopup();
                if (markers.getVisibleParent(layer) != layer){
                    setTimeout(function(){
                        layer.__parent.spiderfy();
                        setTimeout(function(){
                            layer.openPopup();
                        }, 500);
                    }, 1000);
                }
            } else {
                if ('spiderfy' in layer.__parent){
                    setTimeout(function(){
                        layer.__parent.spiderfy();
                        setTimeout(function(){
                            layer.openPopup();
                        }, 500);
                    }, 1000);
                } else {
                    layer.openPopup();
                }
            }
            return;
        }
    });
}

