from django import template
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter
def brization(value):
    return mark_safe(value.replace("\n", "<br/>"))
