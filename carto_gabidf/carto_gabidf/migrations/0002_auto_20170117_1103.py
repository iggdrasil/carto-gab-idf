# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carto_gabidf', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commune',
            name='lat',
            field=models.FloatField(verbose_name='Latitude'),
        ),
        migrations.AlterField(
            model_name='commune',
            name='lon',
            field=models.FloatField(verbose_name='Longitude'),
        ),
        migrations.AlterField(
            model_name='commune',
            name='nom',
            field=models.CharField(verbose_name='Nom', max_length=500),
        ),
    ]
