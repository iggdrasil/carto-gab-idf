# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Commune',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=500, verbose_name=b'Nom')),
                ('lat', models.FloatField(verbose_name=b'Latitude')),
                ('lon', models.FloatField(verbose_name=b'Longitude')),
                ('numero_insee', models.CharField(unique=True, max_length=6, verbose_name='Num\xe9ro INSEE')),
            ],
            options={
                'ordering': ['numero_insee'],
                'verbose_name': 'Commune',
                'verbose_name_plural': 'Communes',
            },
            bases=(models.Model,),
        ),
    ]
