#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from copy import deepcopy
import requests

from django.conf import settings
from django.contrib.gis.geos import fromstr
from django.core.cache import cache
from django.db import models
from django.template.defaultfilters import slugify


class Cache:
    def __init__(self):
        for key in ('raw_json', 'companies', 'produits', 'services'):
            setattr(self, key, self.get_cache(key))
        for key in ('companies_dct', 'comp_by_products', 'comp_by_serv'):
            setattr(self, key, cache.get(key, None))

    def get_cache(self, key):
        cache_key = settings.APP_NAME + "-" + key
        v = cache.get(cache_key)
        if v is not None:
            return v
        v = getattr(self, 'get_' + key)()
        cache.set(cache_key, v)
        return v

    def get_raw_json(self):
        try:
            return requests.get(settings.URL_JSON).json(strict=False)
        except ValueError:
            return []

    def get_produits(self):
        if not self.raw_json or 'features' not in self.raw_json:
            return []
        products = set()
        comp_by_products = {}
        for items in self.raw_json['features']:
            if 'properties' not in items \
                    or 'produits' not in items['properties']:
                continue
            for prod in items["properties"]['produits']:
                products.add(prod)
                slug_prod = slugify(prod)
                if slug_prod not in comp_by_products:
                    comp_by_products[slug_prod] = []
                comp_by_products[slug_prod].append(items['properties']["id"])
        cache.set('comp_by_products', comp_by_products)
        return sorted(products)

    def get_services(self):
        if not self.raw_json or 'features' not in self.raw_json:
            return []
        services = set()
        comp_by_serv = {}
        for items in self.raw_json['features']:
            if 'properties' not in items \
                    or 'services' not in items['properties']:
                continue
            for serv in items["properties"]['services']:
                services.add(serv)
                slug_serv = slugify(serv)
                if slug_serv not in comp_by_serv:
                    comp_by_serv[slug_serv] = []
                comp_by_serv[slug_serv].append(items['properties']["id"])
        cache.set('comp_by_serv', comp_by_serv)
        return sorted(services)

    def get_companies(self):
        if not self.raw_json or 'features' not in self.raw_json:
            return []
        json = deepcopy(self.raw_json)
        companies_dct = {}
        for items in json['features']:
            if 'properties' not in items or \
                    'cp' not in items['properties']:
                continue
            companies_dct[items['properties']["id"]] = deepcopy(items)
            if items['properties'].get('facebook', None) and not items[
                    'properties']['facebook'].startswith('http'):
                companies_dct[items['properties']["id"]]['properties'][
                    'facebook'
                ] = 'https://' + items['properties']['facebook']
            if items['properties'].get('site', None) and not items[
                    'properties']['site'].startswith('http'):
                companies_dct[items['properties']["id"]]['properties'][
                    'site'
                ] = 'http://' + items['properties']['site']
            if items['properties'].get('youtube', None) and not items[
                    'properties']['youtube'].startswith('http'):
                companies_dct[items['properties']["id"]]['properties'][
                    'youtube'
                ] = 'http://' + items['properties']['youtube']
            coord = items['geometry']['coordinates']
            # les coordonnées sont inversées à la source
            point = fromstr('POINT({} {})'.format(coord[1], coord[0]),
                            srid=settings.SRC_SRID)
            point.transform('4326')
            items['geometry']['coordinates'] = (
                point.x, point.y)
            dpt = items['properties'].get('cp')[:2]
            image = settings.STATIC_URL + "images/logo-gab.jpg"
            if 'img' in items['properties'] and \
                    items['properties']['img']:
                image = items['properties']['img']
            companies_dct[items['properties']["id"]]['properties']["image"] = \
                image
            items['properties'] = {
                "id": items['properties'].get('id'),
                "nom": items['properties'].get('nom'),
                "className": "dpt-{}".format(dpt),
                "icon": "marker-dept-{}.png".format(dpt),
                "produits": items['properties'].get('produits', [])
            }
            companies_dct[items['properties']["id"]]['properties']["emails"] = \
                []
            if 'email' in companies_dct[items['properties'][
                    "id"]]['properties']:
                emails = companies_dct[
                    items['properties']["id"]
                ]['properties']['email'].split(' / ')
                for email in emails:
                    companies_dct[items['properties']["id"]]['properties'][
                        "emails"].append(email)

        cache.set('companies_dct', companies_dct)
        return json


class Commune(models.Model):
    nom = models.CharField("Nom", max_length=500)
    lat = models.FloatField("Latitude")
    lon = models.FloatField("Longitude")
    numero_insee = models.CharField(u"Numéro INSEE", max_length=6,
                                    unique=True)

    class Meta:
        verbose_name = u"Commune"
        verbose_name_plural = u"Communes"
        ordering = ['numero_insee']

    def __str__(self):
        return "{} ({})".format(self.nom, self.numero_insee[:2])
