#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json

from django import forms
from django.core.urlresolvers import reverse
from django.forms.widgets import flatatt
from django.template.defaultfilters import slugify
from django.template.loader import render_to_string
from django.utils.html import escape
from django.utils.safestring import mark_safe

from carto_gabidf import models


class JQueryAutoComplete(forms.TextInput):
    TEMPLATE = "blocks/JQueryAutoComplete.html"

    def __init__(self, source, model, options={}, attrs={}):
        """
        Source can be a list containing the autocomplete values or a
        string containing the url used for the request.
        """
        self.options = None
        self.attrs = {}
        self.source = source
        self.model = model
        if len(options) > 0:
            self.options = json.JSONEncoder().encode(options)
        self.attrs.update(attrs)

    def get_source(self):
        return "'{}'".format(reverse(self.source))

    def render(self, name, value=None, attrs=None):
        attrs_hidden = self.build_attrs(attrs, name=name)
        attrs_select = self.build_attrs(attrs)
        selected_value, rendered_value = "", ""

        if value:
            val = escape(value)
            attrs_hidden['value'] = val
            attrs_select['value'] = val
            selected_value = val
            if val:
                try:
                    obj = self.model.objects.get(pk=val)
                    attrs_select['value'] = str(obj)
                    rendered_value = attrs_select['value']
                except:
                    attrs_select['value'] = ""
        if 'id' not in self.attrs:
            attrs_hidden['id'] = 'id_%s' % name
            attrs_select['id'] = 'id_select_%s' % name
        if 'class' not in attrs_select:
            attrs_select['class'] = 'autocomplete'
        dct = {
            'attrs_select': flatatt(attrs_select),
            'attrs_hidden': flatatt(attrs_hidden),
            'field_id': name,
            'min_field_id': name.replace('_', ''),
            'options': self.options,
            'source': self.get_source(),
            'selected_value': selected_value,
            'rendered_value': rendered_value
        }

        return mark_safe(
            render_to_string(self.TEMPLATE, dct))


class SearchForm(forms.Form):
    services = forms.MultipleChoiceField(
        label=u"Services proposés",
        widget=forms.SelectMultiple(attrs={'data-placeholder': u"Tous",
                                           'class': "select2"}),
        choices=[], required=False)
    products = forms.MultipleChoiceField(
        label=u"Produits commercialisés",
        widget=forms.SelectMultiple(attrs={'data-placeholder': u"Tous",
                                           'class': "select2"}),
        choices=[], required=False)
    radius = forms.ChoiceField(label="À moins de", choices=[], required=False)
    localisation = forms.CharField(
        label="De",
        widget=JQueryAutoComplete(
            'autocomplete-town', model=models.Commune,
            attrs={'placeholder': u"Ma commune"}),
        required=False)

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        c = models.Cache()
        self.fields['services'].choices = [(slugify(p), p)
                                           for p in c.get_services() if p]
        self.fields['products'].choices = [(slugify(p), p)
                                           for p in c.get_produits() if p]
        self.fields['radius'].choices = (('10', u'10 kms'),
                                         ('20', u'20 kms'),
                                         ('50', u'50 kms'),
                                         ('+', u'50 kms et au delà'))
