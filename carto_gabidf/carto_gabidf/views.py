#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from copy import deepcopy
from geopy.distance import vincenty
import json
import unicodedata

from django.conf import settings
from django.contrib.gis.geos import fromstr
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.http import JsonResponse, HttpResponse
from django.views.generic import TemplateView
from django.views.generic.edit import FormView

from carto_gabidf import forms, models
from version import get_version


def status(request):
    return HttpResponse('OK')


class JSONResponseMixin(object):
    """
    A mixin that can be used to render a JSON response.
    """
    def render_to_json_response(self, context, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        return JsonResponse(
            self.get_data(context),
            **response_kwargs
        )

    def get_data(self, context):
        """
        Returns an object that will be serialized as JSON by json.dumps().
        """
        return context


class CacheMixin(models.Cache):
    def get_farm_list(self, force=False):
        farm_list = self.request.session.get('farm_list', None)
        if not force and farm_list:
            return farm_list
        data = deepcopy(self.companies)
        filters = {}
        for key in SearchView.form_class.base_fields:
            value = self.request.session.get(key, None)
            if not value:
                continue
            filters[key] = value
        if not data:
            self.request.session['empty'] = True
            return {}
        features_nb = len(data['features'])
        localisation = filters.get('localisation')
        if localisation:
            try:
                localisation = models.Commune.objects.get(pk=localisation)
            except models.Commune.DoesNotExist:
                localisation = None
        for rev_idx, company in enumerate(reversed(data['features'])):
            # reversed pour filtrer d'abord les structures les plus
            # éloignées et ne pas se perdre dans les idx
            cid = company['properties']['id']
            idx = features_nb - 1 - rev_idx
            if cid not in self.companies_dct:
                data['features'].pop(idx)
                continue
            if localisation:
                coord = self.companies_dct[cid]['geometry']['coordinates']
                point = fromstr('POINT({} {})'.format(coord[1], coord[0]),
                                srid=settings.SRC_SRID)
                point.transform('4326')
                distance = round(
                    vincenty((localisation.lon, localisation.lat),
                             (point.x, point.y)).meters / 1000, 2)
                data['features'][idx]['distance'] = distance
                radius = filters.get('radius')
                if radius != '+':
                    radius = int(filters.get('radius') or 0)
                    if radius and distance > radius:
                        data['features'].pop(idx)
                        continue
            if 'services' in filters:
                is_ok = False
                for serv in filters['services']:
                    if serv not in self.comp_by_serv:
                        is_ok = False
                        break
                    if cid in self.comp_by_serv[serv]:
                        is_ok = True
                    else:
                        is_ok = False
                        break
                if not is_ok:
                    data['features'].pop(idx)
                    continue
            if 'products' in filters:
                is_ok = False
                for prod in filters['products']:
                    if prod not in self.comp_by_products:
                        is_ok = False
                        break
                    if cid in self.comp_by_products[prod]:
                        is_ok = True
                    else:
                        is_ok = False
                        break
                if not is_ok:
                    data['features'].pop(idx)
                    continue
        sort_key = lambda x: x['properties']['nom']
        if localisation:
            sort_key = lambda x: x['distance']
        data['features'] = list(sorted(data['features'], key=sort_key))
        self.request.session['farm_list'] = data
        if not data:
            self.request.session['empty'] = True
        else:
            self.request.session['empty'] = False
        return data


class Index(CacheMixin, TemplateView):
    template_name = "index.html"

    def get_context_data(self, *args, **kwargs):
        data = super(Index, self).get_context_data(*args, **kwargs)
        data['API_KEY'] = settings.API_KEY
        data['version'] = get_version()
        return data


class FarmList(CacheMixin, JSONResponseMixin, TemplateView):
    def render_to_response(self, context, **response_kwargs):
        data = self.get_farm_list()
        return self.render_to_json_response(data, **response_kwargs)


class FarmView(CacheMixin, TemplateView):
    template_name = "blocks/popup_detail.html"

    def render_to_response(self, context, **response_kwargs):
        if self.kwargs["company"] not in self.companies_dct:
            return super(FarmView, self).render_to_response(
                context, **response_kwargs)
        context['farm'] = self.companies_dct[self.kwargs["company"]]
        return super(FarmView, self).render_to_response(
            context, **response_kwargs)


class SearchView(CacheMixin, FormView):
    template_name = 'search.html'
    form_class = forms.SearchForm
    success_url = reverse_lazy('search')

    def get_initial(self):
        initial = {}
        for key in self.form_class.base_fields:
            initial[key] = self.request.session.get(key, None)
        return initial

    def form_valid(self, form):
        data = form.cleaned_data
        for key in self.form_class.base_fields:
            self.request.session[key] = data.get(key, None)
        self.get_farm_list(force=True)
        return super(SearchView, self).form_valid(form)

    def get_context_data(self, *args, **kwargs):
        data = super(SearchView, self).get_context_data(*args, **kwargs)
        farm_list = self.get_farm_list(force=True)
        if not farm_list:
            return
        data['farm_list'] = self.get_farm_list(force=True)['features']
        """
        if not self.request.GET.get('first') == 'true':
            data['farm_list'] = self.get_farm_list(force=True)['features']
        else:
            data['first'] = True"""
        return data


def autocomplete_town(request):
    if not request.GET.get('term'):
        return HttpResponse(content_type='text/plain')
    q = request.GET.get('term')
    q = unicodedata.normalize("NFKD", q).encode(
        'ascii', 'ignore').decode('utf-8')
    query = Q()
    for q in q.split(' '):
        extra = Q(nom__icontains=q)
        extra = (extra | Q(numero_insee__istartswith=q))
        query = query & extra
    limit = 20
    towns = models.Commune.objects.filter(query)[:limit]
    data = json.dumps([{'id': town.pk, 'value': str(town)}
                       for town in towns])
    return HttpResponse(data, content_type='text/plain')
