#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib import admin

from django.views.generic import TemplateView

from carto_gabidf import views

urlpatterns = [
    url(r'^$', views.Index.as_view(), name='index'),
    url(r'^status/$', views.status, name='status'),
    url(r'^farms/$', views.FarmList.as_view(), name='farm-list'),
    url(r'^farm/(?P<company>[A-Za-z0-9-]+)/$', views.FarmView.as_view(),
        name='farm-view'),
    url(r'^search/$', views.SearchView.as_view(), name='search'),
    url(r'^autocomplete-town/?$', views.autocomplete_town,
        name='autocomplete-town'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^robots\.txt$',
        TemplateView.as_view(template_name='robots.txt',
                             content_type='text/plain'), name='robots')
]
