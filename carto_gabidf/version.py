VERSION = (0, 9, 3)


def get_version():
    return u'.'.join((str(num) for num in VERSION))

__version__ = get_version()
